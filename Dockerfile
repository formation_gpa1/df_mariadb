FROM mariadb:latest

ADD create_db.sql /docker-entrypoint-initdb.d/

ENV MARIADB_ALLOW_EMPTY_ROOT_PASSWORD yes

